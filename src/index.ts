

const paintingCanvas: HTMLElement=document.querySelector(".painting-canvas");
const drovFigure: HTMLElement=document.querySelector("button");
const drovNewFigure: HTMLElement=document.querySelector("span");

const canvas: any=document.querySelector("#canvas");
let ctx = canvas.getContext('2d');



function addMark(rootEl:HTMLElement,arr:{ x: number, y: number}[],index:number):void{
const localArr:HTMLElement[]=[]
    rootEl.insertAdjacentHTML("afterbegin",`<div class="click-mark click-mark-${index}"><div>`);
    localArr.push(rootEl.querySelector(`.click-mark-${index}`));
    localArr[0].textContent=`${index+1}`
    localArr[0].style.left=`${arr[index].x}px`;
    localArr[0].style.top=`${arr[index].y}px`;
}
function renderView(area:number,perimeter:number){
    const describe: HTMLElement=document.querySelector("p");
    describe.textContent=`
    the area of your figure is ${area}px,
    perimeter is ${perimeter}px`
    drovNewFigure.style.display="block";
    drovFigure.style.display="none";

}

//trial interface.
interface IcliksReaction{
    coordinates:{ x: number, y: number}[];  
}

class HandlerUsersClicks implements IcliksReaction{
    coordinates:{ x: number, y: number}[]=[];    
     constructor(){
         this.getClickСoordinates();
     }
    getClickСoordinates(){
        let index:number=0;   
        paintingCanvas.onclick=(e)=>{
            console.log(paintingCanvas.getBoundingClientRect())                      
            this.coordinates.push({
                 x:e.clientX-8,
                 y:e.clientY-26,
                });
                addMark(paintingCanvas,this.coordinates,index);
                index+=1
        }  
    } 
    getAllСoordinates(){        
    return this.coordinates     
    }
}
class DrovFigure{
   
    constructor(arr:{ x: number, y: number}[]){
       this.action(arr);
    }
    action(arr:{ x: number, y: number}[]){
        drovFigure.onclick=()=>{
        paintingCanvas.style.display="none";
        canvas.style.display="block";
        this.createFigure(arr);
        }           
    }
    createFigure(arr:{ x: number, y: number}[]){
        ctx.beginPath();
        arr.forEach((value)=>{
            ctx.lineTo(value.x,value.y);
        })
        ctx.lineTo(arr[0].x,arr[0].y);
        ctx.fill();
    }
}  
class СalculateFigureValues{
    arr:number[]=[];    
    constructor(data:{ x: number, y: number}[]){
        this.action(data);    
    }

    action(data:{ x: number, y: number}[]){
        drovFigure.addEventListener("click",()=>{
            this.getArea();
            this.getPerimetr(data);
            renderView(this.arr[0],this.arr[1]);
        })
    }

    getArea(){
        let result:number=0;
        ctx.getImageData(9,28,300,150).data.forEach((e:number)=>{
              if(e!==0){
                result+=1
            }
        })
       this.arr.push(result);
    } 
    getPerimetr(arr:{ x: number, y: number}[]){

        arr.push(arr[0]);
        const arrLength:number[]=[];
        let result:number=0;

        arr.forEach((value,index)=>{
        if(arr[index+1]!==undefined){
        arrLength.push(Math.sqrt(Math.pow((arr[index+1].x)-(arr[index].x), 2)+Math.pow(arr[index+1].y-arr[index].y,2)));
        }      
        });
        arrLength.forEach((value)=>{
            result+=value
        });
        this.arr.push(result);
    }
}

class Aplication{
initialize(){
    let clicksInfo:HandlerUsersClicks;
    clicksInfo= new HandlerUsersClicks();

    let drowFigure:DrovFigure;
    drowFigure= new DrovFigure(clicksInfo.getAllСoordinates());


    let dataOfFigura:СalculateFigureValues;
    dataOfFigura= new СalculateFigureValues(clicksInfo.getAllСoordinates());

    this.action();
}
action(){
    drovNewFigure.addEventListener("click",()=>{
        window.location.reload();
    })
}

}



let app:Aplication;
app =new Aplication();
app.initialize();


